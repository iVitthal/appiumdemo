Given("I land on Home screen") do
  sleep 2
  text("Minimal")
  find_element(id: "addToDoItemFAB")
  puts "I have landed on Home screen"
end

When("I click on add note button on home screen") do
  sleep 0.5
  find_element(id: "addToDoItemFAB")
  find_element(id: "addToDoItemFAB").click
  puts "I have clicked on add note button on home screen"
end

Then("I should able to see add note screen") do
  sleep 1
  find_element(id: "toDoCustomTextInput")
  find_element(id: "makeToDoFloatingActionButton")
  puts "I am able to see add note screen"
end

When("I add title to note as {string}") do |title|
  sleep 1
  find_element(id: "userToDoEditText")
  find_element(id: "userToDoEditText").click
  find_element(id: "userToDoEditText").clear
  find_element(id: "userToDoEditText").send_keys(title)
  hide_keyboard
  puts "I have added title to note as " + title
end

When("I add description as {string}") do |description|
  sleep 0.5
  find_element(id: "userToDoDescription")
  find_element(id: "userToDoDescription").click
  find_element(id: "userToDoDescription").clear
  find_element(id: "userToDoDescription").send_keys(description)
  hide_keyboard
  puts "I have added description as " + description
end

When("I click on add note button on add note screen") do
  sleep 0.5
  find_element(id: "makeToDoFloatingActionButton")
  find_element(id: "makeToDoFloatingActionButton").click
  sleep 1
  find_element(id: "addToDoItemFAB")
  puts "I have clicked on add note button on add note screen"
end
Then("I should able to see note {string} is added to list") do |title|
  sleep 1
  find_element(id: "addToDoItemFAB")
  text(title)
  sleep 1
  puts "I am able to see note '" + title + "' is added to list"
end

When("I update title of note as {string}") do |updated_title|
  sleep 1
  find_element(id: "userToDoEditText")
  find_element(id: "userToDoEditText").click
  find_element(id: "userToDoEditText").clear
  find_element(id: "userToDoEditText").send_keys(updated_title)
  hide_keyboard
end

Then("I should able to see note {string} is updated in list") do |updated_title|
  sleep 1
  find_element(id: "addToDoItemFAB")
  sleep 1
  text(updated_title)
  sleep 1
  puts "I am able to see note '" + updated_title + "' is updated in list"
end

When("I click on first note on screen") do
  sleep 1
  find_element(id: "toDoListItemColorImageView")
  find_element(id: "toDoListItemColorImageView").click
  puts "I have clicked on first note on screen"
end
When("I swipe on first note from left to right") do
  width = window_size[:width]
  width = width / 2
  height = window_size[:height]
  height = height / 2
  Appium::TouchAction.new.swipe(start_x: 60, start_y: 200, end_x: 600, end_y: 200, duration: 500).perform
end

Then("I should able to see note is deleted") do
  sleep 1
  find_element(id: "snackbar_action")
  puts "I am able to see note is deleted"
end

When("I add reminder as tomorrow") do
  sleep 1
  find_element(id: "toDoHasDateSwitchCompat")
  find_element(id: "toDoHasDateSwitchCompat").click

  find_element(id: "newTodoDateEditText")
  find_element(id: "newTodoDateEditText").click

  dates = find_elements(class: "android.view.View")
  $selected_index = 1
  dates.each_with_index { |date, i|
    $selected_index = 1
    if date.selected?
      $selected_index = i
    end
  }
  dates[$selected_index + 3].click

  find_element(id: "ok")
  find_element(id: "ok").click
end

When("I click on options button") do
  sleep 1
  find_element(id: "toolbar").find_element(class: "android.widget.ImageView")
  find_element(id: "toolbar").find_element(class: "android.widget.ImageView").click
  puts "I have clicked on options button"
end

When("I click on settings button") do
  sleep 1
  text("Settings")
  text("Settings").click
  puts "I have clicked on settings button"
end

Then("I should able to see settings screen") do
  sleep 1
  find_element(class: "android.widget.CheckBox")
  $old_night_mode_status = find_element(id: "android:id/summary").text
  puts "I am able to see settings screen"
end

When("I click on checkbox to toggle night mode") do
  sleep 1
  find_element(class: "android.widget.CheckBox")
  find_element(class: "android.widget.CheckBox").click
  sleep 0.5
  puts "I have clicked on checkbox to toggle night mode"
end

Then("I should able to see night mode is toggled") do
  sleep 3
  $current_night_mode_status = find_element(id: "android:id/summary").text
  if ($current_night_mode_status == $old_night_mode_status)
    fail("I am not able to see night mode is toggled")
  end
  puts "I am able to see night mode is toggled"
end
When("I click on about button") do
  sleep 1
  text("About")
  text("About").click
  puts "I have clicked on about button"
end

Then("I should able to see about screen") do
  sleep 0.5
  find_element(id: "aboutVersionTextView")
  find_element(id: "aboutContactMe")
  puts "I am able to see about screen"
end
