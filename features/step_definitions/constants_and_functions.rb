
def hide_keyboard
  if @driver.is_keyboard_shown
    $driver.hide_keyboard
  end
end

# def refresh_screen_element
#   if !$iOS_platform
#     press_keycode(187)
#     sleep 1
#     back
#     hide_error_temp
#   end
# end

# def get_platform_name
#   if $driver.device_is_android?
#     $platformName = "Android"
#     $iOS_platform = false
#   else
#     $platformName = "iOS"
#     $iOS_platform = true
#   end
# end

# def hide_keyboard
#   if $iOS_platform
#     begin
#       find_element(class:'XCUIElementTypeKeyboard').find_elements(class:'XCUIElementTypeButton').last.click
#     rescue
#       # Do nothing
#     end
#   else
#     if @driver.is_keyboard_shown
#       $driver.hide_keyboard
#     end
#   end
# end

# def check_login
#   sleep 0.1
#   if $iOS_platform
#     begin
#       find_element(accessibility_id:'Welcome to BuddyConnect™')
#         $is_logged_in = false
#     rescue
#       $is_logged_in = true
#     end
#   else
#     begin
#       find_element(id:'getStartedBtn')
#       $is_logged_in = false
#     rescue
#       $is_logged_in = true
#     end
#   end
# end

# def hide_tutorial_button
#   if !$iOS_platform
#     begin
#       find_element(id:'tutorialGotItBtn').click
#     rescue
#       # Nothing happenes here
#     end
#   end
# end

# def hide_error_temp
#   sleep 0.1
#   if $iOS_platform
#   else
#     begin
#       find_elements(class:'android.widget.TextView')[1].text == 'Something went wrong. Please try again.'
#       find_element(id:'genericNeutralBtn').click
#     rescue
#       #nothing
#     end
#   end
# end

# def wait_till_processing_indicator_disappear
#   if $iOS_platform
#     loop do
#       $indicator = find_elements(accessibility_id:'SVProgressHUD')
#       if $indicator.size == 0
#         break
#       end
#     end
#   else
#     loop do
#       $indicator = $driver.find_elements(class:'android.widget.ProgressBar')
#       if $indicator.size == 0
#         break
#       end
#     end
#   end
# end

# def search_by_text(text)
#   begin
#     element = text(text).text
#   rescue
#     element = nil
#   end
#   element
# end

# def accept_alert
#   if $iOS_platform
#     begin
#       $driver.alert_accept
#     rescue => exception
#       #nothing to do
#     end
#   end
# end

# def do_swipe_up
#   width = window_size[:width]
#   width = width / 2
#   height = window_size[:height]
#   height = height / 2
#   Appium::TouchAction.new.swipe(start_x: width, start_y: height, end_x: width, end_y: 20, duration: 500).perform
# end

# # def wait_till_pin_code_search
# #   loop do
# #     $indicator = $driver.find_elements(class: 'android.widget.ProgressDialog')
# #     if $indicator.size == 0
# #       break
# #     end
# #   end
# # end

# # def wait_till_app_comes_online
# #   loop do
# #     $indicator = $driver.find_elements(id: 'tv_offline')
# #     if $indicator.size == 0
# #       break
# #     end
# #   end
# # end

# # def is_team_manager
# #   sleep 0.001
# #   flag = false
# #   if $iOS_platform
# #     if (find_element(class:'XCUIElementTypeTabBar').find_elements(class:'XCUIElementTypeButton').size == 5)
# #     flag = true
# #     end
# #   else
# #     item = find_element(id: 'bottom_navigation').find_element(class: 'android.view.ViewGroup').find_elements(class: 'android.widget.FrameLayout')
# #     if item.size == 5
# #       flag = true
# #     end
# #   end
# #   check_login
# #   flag
# # end

# # def is_landed_on_login_screen
# #   sleep 0.001
# #   flag = false
# #   if $iOS_platform
# #     begin
# #       if text('Welcome to the Macmillan volunteering app')
# #         flag = true
# #         $is_logged_in = false
# #       end
# #     rescue
# #       if text('Hours volunteered for Macmillan')
# #         flag = false
# #         $is_logged_in = true
# #       end
# #     end
# #   end
# #   if !$iOS_platform
# #     begin
# #       if text('Login')
# #         flag = true
# #         $is_logged_in = false
# #       end
# #     rescue
# #       if text('My Account')
# #         flag = false
# #         $is_logged_in = true
# #       end
# #     end
# #   end
# #   flag
# # end

# # def do_login
# #   wait_till_processing_indicator_disappear
# #   sleep 1
# #   if $iOS_platform
# #     username_field = find_element(class:'XCUIElementTypeTextField')
# #     username_field.clear
# #     username_field .send_keys($username)
# #     pwd_field = find_element(class:'XCUIElementTypeSecureTextField')
# #     pwd_field.clear
# #     pwd_field.send_keys($current_password)
# #     hide_keyboard
# #     find_element(xpath:'//XCUIElementTypeButton[@name="Login"]').click
# #   else
# #     find_elements(class: 'android.widget.EditText')[0].clear
# #     find_elements(class: 'android.widget.EditText')[0].send_keys($username)
# #     find_elements(class: 'android.widget.EditText')[1].clear
# #     find_elements(class: 'android.widget.EditText')[1].send_keys($current_password)
# #     hide_keyboard
# #     find_element(class: 'android.widget.Button').click
# #   end
# #   wait_till_processing_indicator_disappear
# #   puts('Logged In')
# # end

# # def do_logout_from_any_screen_except_my_profile
# #   sleep 0.001
# #   wait_till_processing_indicator_disappear
# #   sleep 0.001
# #   find_element(id: 'Iv_myacc').click
# #   wait_till_processing_indicator_disappear
# #   sleep 0.001
# #   wait_till_processing_indicator_disappear
# #   text('Sign out').click
# # end

# # def do_login_from_discover_screen_as_team_manager
# #   sleep 0.001
# #   wait_till_processing_indicator_disappear
# #   sleep 0.001
# #   find_element(id: 'Iv_myacc').click
# #   sleep 0.001
# #   find_elements(class: 'android.widget.EditText')[0].clear
# #   find_elements(class: 'android.widget.EditText')[0].send_keys($team_manager_username)
# #   find_elements(class: 'android.widget.EditText')[1].clear
# #   find_elements(class: 'android.widget.EditText')[1].send_keys($team_manager_current_password)
# #   hide_keyboard
# #   find_element(class: 'android.widget.Button').click
# #   wait_till_processing_indicator_disappear
# #   puts('Logged In')
# # end

# # def do_login_from_discover_screen
# #   sleep 0.001
# #   wait_till_processing_indicator_disappear
# #   sleep 0.001
# #   find_element(id: 'Iv_myacc').click
# #   sleep 0.001
# #   find_elements(class: 'android.widget.EditText')[0].clear
# #   find_elements(class: 'android.widget.EditText')[0].send_keys($username)
# #   find_elements(class: 'android.widget.EditText')[1].clear
# #   find_elements(class: 'android.widget.EditText')[1].send_keys($current_password)
# #   hide_keyboard
# #   find_element(class: 'android.widget.Button').click
# #   wait_till_processing_indicator_disappear
# #   puts('Logged In')
# # end

# # def check_for_toggle
# #   if ($cont_pref_opp_alert_toggle1 == 'ON')
# #     $cont_pref_opp_alert_toggle_is_on = true
# #   end
# #   if ($cont_pref_opp_alert_toggle1 != 'ON')
# #     $cont_pref_opp_alert_toggle_is_on = false
# #   end
# # end

# # def turn_on_wifi
# #   #As sim is not in device data will always return true but actual values are as below
# #   #0 (None), 1 (Airplane Mode), 2 (Wifi only), 4 (Data only) & 6 (All network on)
# #   if ($driver.get_network_connection == 6 || $driver.get_network_connection == '6')
# #     puts 'Wifi is already on'
# #   end
# #   if ($driver.get_network_connection == 4 || $driver.get_network_connection == '4')
# #     $driver.toggle_wifi
# #     puts 'Wifi is turned on'
# #   end
# # end

# # def turn_off_wifi
# #   #As sim is not in device data will always return true but actual values are as below
# #   #0 (None), 1 (Airplane Mode), 2 (Wifi only), 4 (Data only) & 6 (All network on)
# #   if ($driver.get_network_connection == 4 || $driver.get_network_connection == '4')
# #     puts 'Wifi is already off'
# #   end
# #   if ($driver.get_network_connection == 6 || $driver.get_network_connection == '6')
# #     $driver.toggle_wifi
# #     puts 'Wifi is turned off'
# #   end
# # end

# # def do_go_online
# #   #As sim is not in device data will always return true but actual values are as below
# #   #0 (None), 1 (Airplane Mode), 2 (Wifi only), 4 (Data only) & 6 (All network on)
# #   if ($driver.get_network_connection == 4 || $driver.get_network_connection == '4')
# #     $driver.toggle_wifi
# #   end
# #   txts = find_elements(class: 'android.widget.TextView')
# #   if (txts[1].text == 'You are offline.')
# #     find_element(id: 'btnOk').click
# #     width = window_size[:width]
# #     width = width / 2
# #     height = window_size[:height]
# #     height = height / 2
# #     do_swipe_down
# #     wait_till_app_comes_online
# #   end
# #   wait_till_processing_indicator_disappear
# # end

# # def do_swipe_down
# #   width = window_size[:width]
# #   width = width / 2
# #   height = window_size[:height]
# #   height = height / 2
# #   Appium::TouchAction.new.swipe(start_x: width, start_y: height, end_x: width, end_y: height * 1.9, duration: 500).perform
# # end

# # def login_after_session_expired
# #   find_element(id: 'btnOk').click
# #   find_elements(class: 'android.widget.EditText')[0].clear
# #   find_elements(class: 'android.widget.EditText')[0].send_keys($username)
# #   find_elements(class: 'android.widget.EditText')[1].clear
# #   find_elements(class: 'android.widget.EditText')[1].send_keys($current_password)
# #   hide_keyboard
# #   find_element(class: 'android.widget.Button').click
# #   wait_till_processing_indicator_disappear
# # end

# # def login_after_session_expired_iOS
# #   ok_btns = find_elements(xpath: '//XCUIElementTypeButton[@name="OK"]')
# #   if ok_btns.size != 0
# #     ok_btns[0].click
# #   end
# #   wait_till_processing_indicator_disappear
# #   sleep 0.1
# #   find_element(accessibility_id: 'SignUp/SignIn').click
# #   username_field = find_element(class:'XCUIElementTypeTextField')
# #   username_field.clear
# #   username_field .send_keys($username)
# #   pwd_field = find_element(class:'XCUIElementTypeSecureTextField')
# #   pwd_field.clear
# #   pwd_field.send_keys($current_password)
# #   hide_keyboard
# #   find_element(xpath:'//XCUIElementTypeButton[@name="Login"]').click
# #   wait_till_processing_indicator_disappear
# # end

# ####Below are the global constants/variables declared in project########
# # $platformName
# # $username
# # $password
# # $platformName
# # $is_logged_in
# # $indicator
# # $cont_pref_opp_alert_toggle1
# # $cont_pref_opp_alert_toggle
# # $cont_pref_vol_newsletter_toggle
# # $cont_pref_cont_by_email_toggle
# # $cont_pref_cont_by_phone_toggle
# # $cont_pref_cont_by_text_toggle
# # $cont_pref_cont_by_post_toggle
# # $cont_pref_opp_alert_toggle_is_on
# # $no_search_results
# # $iOS_platform
# #$twitter_tag_present
# #######################################################
