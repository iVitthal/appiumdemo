require "appium_lib"
require "selenium-webdriver"
require "fileutils"
require "pry"

def android_caps
  {
    caps: {
      deviceName: "AnyName",
      platformName: "Android",
      app: (File.join(File.dirname(__FILE__), "AppiumDemoApp.apk")),
      newCommandTimeout: 20000,
      appPackage: "com.avjindersinghsekhon.minimaltodo",
      noReset: true,
      fullReset: false,
      dataReset: false,
      automationName: "UIAutomator2",
      autoGrantPermissions: true,
    },
  }
end

Appium::Driver.new(android_caps, true)

Appium.promote_appium_methods Object
