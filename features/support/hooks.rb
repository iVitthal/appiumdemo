Before do
  $driver.start_driver
end

After do |scenario|
  if !File.directory?("Android_Screenshots")
    FileUtils.mkdir_p("Android_Screenshots")
  end
  time_stamp = Time.now.strftime("%Y-%m-%d_%H.%M.%S")
  screenshot_name = time_stamp + ".png"
  screenshot_file = File.join("Android_Screenshots", screenshot_name)
  $driver.screenshot(screenshot_file)
  embed("#{screenshot_file}", "image/png")
  $driver.driver_quit
end

AfterConfiguration do
  FileUtils.rm_r("Android_Screenshots") if File.directory?("Android_Screenshots")
end
