@application
Feature: To check various functionalities of app
   # The word guess game is a turn-based game for two players.
   # The Maker makes a word for the Breaker to guess. The game
   # is over when the Breaker guesses the Maker's word.

   # Example: Maker starts a game

   Scenario: User should able to add note
      Given I land on Home screen
      When I click on add note button on home screen
      Then I should able to see add note screen
      When I add title to note as 'Note Title'
      And I add description as 'Note Description'
      And I click on add note button on add note screen
      Then I should able to see note 'Note Title' is added to list

   Scenario: User should able to update note
      Given I land on Home screen
      When I click on first note on screen
      Then I should able to see add note screen
      When I update title of note as 'Updated Note Title'
      And I add description as 'Updated Note Description'
      And I click on add note button on add note screen
      Then I should able to see note 'Updated Note Title' is updated in list

   Scenario: User should able to delete note
      Given I land on Home screen
      When I swipe on first note from left to right
      Then I should able to see note is deleted

   Scenario: User should able to update note with reminder
      Given I land on Home screen
      When I click on first note on screen
      Then I should able to see add note screen
      When I update title of note as 'Updated Note Title'
      And I add description as 'Updated Note Description'
      And I add reminder as tomorrow
      And I click on add note button on add note screen
      Then I should able to see note 'Updated Note Title' is updated in list

   Scenario: User should able to see settings screen
      Given I land on Home screen
      When I click on options button
      And I click on settings button
      Then I should able to see settings screen

   Scenario: User should able to toggle night mode settings
      Given I land on Home screen
      When I click on options button
      And I click on settings button
      Then I should able to see settings screen
      When I click on checkbox to toggle night mode
      Then I should able to see night mode is toggled

   Scenario: User should able to see about screen
      Given I land on Home screen
      When I click on options button
      And I click on about button
      Then I should able to see about screen
   @wip
   Scenario Outline: User should able to add multiple note
      Given I land on Home screen
      When I click on add note button on home screen
      Then I should able to see add note screen
      When I add title to note as <title>
      And I add description as <description>
      And I click on add note button on add note screen
      Then I should able to see note <title> is added to list
      Examples:
         | title         | description         |
         | 'Note Title1' | 'Note Description1' |
         | 'Note Title2' | 'Note Description2' |